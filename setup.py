from setuptools import setup

__version__ = '0.0.1'

setup(
    name='faceduct',
    url='https://gitlab.com/eyeo/machine-learning/faceduct',
    author='eyeo GmbH',
    author_email='info@adblockplus.org',
    packages=['faceduct'],
    version=__version__,
)
