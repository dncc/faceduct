import binascii
import os
import pickle
import random
import tempfile
from collections import defaultdict
from typing import Generator, List, Set, Tuple

from faceduct.constants import HEADERS


SHINGLE_SIZE = 2
MAX_SHIGLE_ID = 2**32-1
"""
First prime larger than 2^32-1:
    http://compoasso.free.fr/primelistweb/page/prime/liste_online_en.php
"""
NEXT_PRIME = 4294967311


class MinHashSignature:
    def __init__(self, dim: int = 128, seed: int = 1):
        self.dim = dim
        coeffs = self._hash_coeffs(2 * dim, seed)
        coeffs = list(zip(coeffs[:dim], coeffs[dim:]))
        self._hashes = [self._ihash(*coeffs[i]) for i in range(dim)]


    def _hash_coeffs(self, dim: int, seed: int) -> List[int]:
        coeffs = set()
        k = dim
        random.seed(seed)
        while k > 0:
            coef = random.randint(0, MAX_SHIGLE_ID)
            while coef in coeffs:
                coef = random.randint(0, MAX_SHIGLE_ID)

            coeffs.add(coef)
            k -= 1

        return list(coeffs)


    def _ihash(self, a: int, b: int) -> int:
        return lambda val: (a * val + b) % NEXT_PRIME


    def sign(self, s: set) -> List[int]:
        return [min(h(t) for t in s) for h in self._hashes]


class LSH:
    """
    The algorithm is based on "Mining of Massive Datasets" (MMDS), by Leskovec, Rajaraman and Ullman:

        http://infolab.stanford.edu/~ullman/mmds.html (section 3.4)

    It uses banding approach to group similar minhash signatures into the same bucket,
    in two steps:
        1. Generate a minhash signature for each document
        2. Map similar signatures to same buckets by using the banding technique (section 3.4.2)

    """
    def __init__(self, length: int = 128, threshold: float = 0.5, shingle_size=SHINGLE_SIZE, seed: int = 1):
        """
        :param length: minhash signature length
        :param threshold: jaccard similarity threshold for two documents
        :param shingle_size: # of words in a shingle, 2 is default, the more
                words in the shingle the more conservative becomes the similarity
                metric
        :param seed: a random generator seed for Minhash signature hashing functions
        """
        if threshold > 1.0 or threshold < 0.0:
            raise ValueError("Similarity threshold should be in [0, 1] range.")

        self.length = length
        self.threshold = threshold
        self.bandwidth = self._get_bandwidth(length, threshold)
        self.num_bands = self.get_num_bands()
        self.lsh_index = [defaultdict(set) for _ in range(self.num_bands)]
        self.shingle_size = shingle_size
        self.minhash = MinHashSignature(dim=length, seed=seed)


    def _get_bandwidth(self, n: int, s: float) -> int:

        """
        :param n: minhash signature length
        :param s: jaccard similarity threshold for two documents
        :return: bandwidth, an approximate number of rows in each LSH band

        Given the signature length (n) and similarity threshold (s) between two documents
        approximate the number of rows (r) in each band (b) needed to reach the threshold
        (s). Using the formula from MMDS (section 3.4.2):

            s = (1 / b) ** (1 / r)

        """
        minerr = n
        bandwidth = n
        for r in range(1, n+1):
            try:
                b = 1.0 / s ** r
            except:
                return bandwidth
            err = abs(n - b * r)
            if err < minerr:
                minerr = err
                bandwidth = r

        return bandwidth


    def get_threshold(self) -> float:
        """:return: similarity threshold, an approximate value of the set `self.threshold`"""
        b = self.length / float(self.bandwidth)

        return (1.0 / b) ** (1.0 / self.bandwidth)


    def get_num_bands(self) -> int:
        """:return: number of bands, each with `self.bandwidth` number of rows/values"""
        return self.length // self.bandwidth


    def _shingle(self, doc: str, k: int) -> Generator[str, None, None]:
        """Shingle a document by combining `k` words together."""
        words = doc.strip().split()
        k = min(len(words), k)
        for i in range(len(words) - k + 1):
            yield " ".join(words[i:i+k])


    def _shingle_ids(self, doc: str, k: int) -> Generator[int, None, None]:
        """First k-shingle a document, then hash the shingles to 32-bit integers."""
        for s in self._shingle(doc, k):
            yield binascii.crc32(s.encode('utf-8')) & 0xffffff


    def to_lsh(self, doc: str, k: int = None) -> Generator[int, None, None]:
        """Create a minhash signature and hash its each band"""
        r = self.bandwidth
        k = k or self.shingle_size
        shingled_doc = set([s for s in self._shingle_ids(doc, k)])
        sig = self.minhash.sign(shingled_doc)
        for band in zip(*(iter(sig),) * r):
            yield sum(band) % NEXT_PRIME


    def index(self, dockey: str, doc: str, k: int = None) -> None:
        """Index/Add a (key, doc) pair to the LSH cache"""
        for band_idx, lsh_hash in enumerate(self.to_lsh(doc)):
            self.lsh_index[band_idx][lsh_hash].add(dockey)


    def index_batch(self, key_doc_tuples: List[Tuple[str, str]]) -> None:
        """Index/Add (key, doc) pairs in batch to the LSH cache"""
        for i, key_doc_tup in enumerate(key_doc_tuples):
            self.index(*key_doc_tup)


    def get_duplicates(self, doc: str, dockey: str = None) -> Set[str]:
        dups = set()
        for band_idx, lsh_hash in enumerate(self.to_lsh(doc)):
            dups |= self.lsh_index[band_idx][lsh_hash]

        if dockey and dockey in dups:
            dups.remove(dockey)

        return dups


    def get_buckets(self, doc: str, dockey: str = None) -> List[Tuple[int, int, set]]:
        buckets = []
        for band_idx, lsh_hash in enumerate(self.to_lsh(doc)):
            buckets.append((band_idx, lsh_hash, self.lsh_index[band_idx][lsh_hash]))

        return buckets


    def dump(self, output_file: str = 'lsh.db', output_dir: str = None) -> str:
        if not output_dir:
            output_dir = tempfile.gettempdir()

        if not os.path.exists(output_dir):
            raise Exception("Output dir. {} doesn't exist".format(output_dir))

        file_path = os.path.sep.join([output_dir, output_file])
        with open(file_path, 'wb') as out:
            pickle.dump(self.lsh_index, out)

        return file_path


    def load(self, input_file: str = 'lsh.db', input_dir: str = None) -> None:
        if not input_dir:
            input_dir = tempfile.gettempdir()

        if not os.path.exists(input_dir):
            raise Exception("Input dir. {} doesn't exist".format(input_dir))

        file_path = os.path.sep.join([input_dir, input_file])
        with open(file_path, 'rb') as f:
            self.lsh_index = pickle.load(f)


def index(input_file: str, output_file: str, length=128, threshold=0.4, shingle_size=2, skip_headers=True) -> Set[str]:
    lsh = LSH(length=length, threshold=threshold, shingle_size=shingle_size)
    print("LSH Indexing ...")
    count = count_dup = 0
    seen = set()
    uniq = set()
    uniq_file = open(output_file, 'w')
    uniq_file.write(','.join(HEADERS) + '\n')
    print("Saving unique text snippets to: {}".format(uniq_file.name))
    with open(input_file) as f:
        if skip_headers:
            next(f)
        for line in f:
            try:
                filename, post, label, sponsored = line.strip().split(',')
            except:
                print("Should have: {}".format(HEADERS))
                print("Found: {}".format(line))
                continue
            if filename in seen:
                continue
            if filename == '' or filename is None:
                continue
            count+=1
            seen.add(filename)
            lsh.index(filename, post)
            if len(lsh.get_duplicates(post, filename)) > 0:
                count_dup += 1
                continue
            uniq_file.write(line)
            uniq.add(filename)
            if count % 1000 == 0:
                print("{} indexed".format(count))

    uniq_file.close()

    return count, count_dup, uniq, lsh


if __name__ == '__main__':
    lsh = LSH(length=128, threshold=0.4, shingle_size=2)

    docs = [
        "greg williams photography over 30 years i have developed a system for delivering great pictures in any scenario  the system is quick  simple and can be learned by anyone 0:04 0:58 skillsfaster com greg williams candid photography skills ver más 4 4 mil 4 4 mil 149 comentarios 251 veces compartido me gusta comentar compartir",
        "greg williams photography over 30 years i have developed a system for delivering great pictures in any scenario  the system is quick  simple and can be learned by anyone 0:05 0:58 skillsfaster com greg williams candid photography skills ver más 4 4 mil 4 4 mil 149 comentarios 251 veces compartido me gusta comentar compartir",
        "greg williams photography over 30 years i have developed a system for delivering great pictures in any scenario  the system is quick  simple and can be learned by anyone 0:00 0:58 skillsfaster com greg williams candid photography skills ver más",
        "design crowdfunding projects both sides are now the cool side kickstarter com/always-cool-pillow no more pillow flipping! pre-order and save for a limited time 1 6 mil 1 6 mil 747 comentarios 159 veces compartido me gusta comentar compartir",
        "design crowdfunding projects both sides are now the cool side kickstarter com/always-cool-pillow no more pillow flipping! pre-order and save for a limited time 1 8 mil 1 8 mil 817 comentarios 170 veces compartido me gusta comentar compartir",
        "domestika discover our new courses and learn from top professionals in the creative field  introduction to photoshop for illustrators a course by gemma gould creative bullet journal: planning and creativity a course by little hannah 3d animation with cinema 4d and redshift for beginners a course by farid ghanbari (renderburger) adobe illustrator for beginners a course by tina touli adobe after effects for beginners a course by manuel neto illustrating and writing a childrens book a course by valentina toro adobe lightroom classic: a beginners guide a course by mikael eliasson data visualization for editorial projects a course by diana estefanía rubio exploratory sketchbook: find your drawing style a course by sarah van dongen natural landscapes in watercolor a course by daniel pito campos see more at domestika org",
        "domestika discover our new courses and keep learning from top professionals in the creative field  sketchbook illustration for collecting ideas a course by catalina bu portfolio animation with after effects a course by holke 79 lighting and color for digital portraits in photoshop a course by karmen loh (bearbrickjia) e-commerce fundamentals: launching an online store from scratch a course by ellie rivers wordpress: create a professional website from scratch a course by anyssa ferreira introduction to tiktok for creatives a course by that icelandic guy digital character painting with procreate a course by erick hernández anda color theory applied to online projects a course by james eccleston 3d typography: playing with color and volume a course by thomas burden designing and prototyping your first chair a course by muka design lab see more at domestika org",
        "wir sind flaschenpost de 0:01 0:10 wirsindflaschenpost de auslieferungsfahrer (m/w/d) apply now",
        "wir sind flaschenpost de 0:01 0:15 wirsindflaschenpost de auslieferungsfahrer (m/w/d) | wirsindflaschenpost de apply now",
        ]

    for i, doc in enumerate(docs):
        lsh.index(str(i), doc)

    for i in [0, 3, 5, 7]:
        doc =  docs[i]
        dups = lsh.get_duplicates(doc, str(i))
        print("===")
        print("Dups for:")
        print("---")
        print('{}. {}'.format(str(i), doc))
        print("---")
        if len(dups) == 0:
            print("No duplicates")
            continue
        for docid in dups:
            print('{}. {}'.format(docid, docs[int(docid)]))
