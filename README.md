# Data Pipeline for Facebook Ads Classification

## Removing Duplicate Facebook Posts

To detect and remove duplicates, run:

```bash
python faceduct/dedup.py <input_dir> <output_dir>  -w <number_of_workers>
```

The detection and removal will be done in the following steps:

1. HTML Parsing: parse Facebook HTML posts (expected to be in `<input_dir>`) and
   extract their text content into a separate file (CSV, it'll be saved in
   `<output_dir>`).
2. LSH Indexing: use parsed text from the 1st step to detect duplicates by using
   LSH fuzzy matching.
3. Save: save unique HTML and JSON files (detected in the 2nd step) from
   `<output_dir>` to `<input_dir>`

If the task has finished successfully, you should see something like the
following:

```bash

Parsing has finished successfully!
========== REPORT =========
total # of posts: 50
# of parsed posts: 50
# of skipped posts: 0
# of ad posts: 6

Ratio parsed/total : 100.0%
Ratio ads/parsed : 12.0%

Time: 14.4 sec == 0.24 min == 0.0 h

LSH Indexing ...
Saving unique text snippets to: ../data/fb-out/snippets.dedup.csv
Saving LSH index db to: /tmp/lsh.db
Saving unique posts to ../data/fb-out ...

Removing duplicates has finished!
========== REPORT =========
total # of posts: 50
# of unique posts: 49
# of duplicate posts: 1

Ratio unique/total : 98.0%
Ratio duplicate/total : 2.0%

Total duration: 38.98 sec == 0.65 min == 0.01 h

```

To get the full list of arguments with descriptions for this task, run:
```bash
python faceduct/dedup.py --help
```

## Parsing Facebook HTML Posts

If you just want to extract text snippets from Facebook HTML files, run:

```python
python faceduct/html2txt.py <input-dir> <output-file> -w <num_workers>
```

It will parse Facebook HTML posts and extract their text content into a separate file (CSV).

If the task has finished successfully, you should see something like the
following:

```
Parsing has finished successfully!
========== REPORT =========
total # of posts: 50
# of parsed posts: 50
# of skipped posts: 0
# of ad posts: 6

Ratio parsed/total : 100.0%
Ratio ads/parsed : 12.0%

Time: 24.57 sec == 0.41 min == 0.01 h
```

To get the full list of arguments with descriptions for this task, run:
```bash
python faceduct/html2txt.py --help
```
