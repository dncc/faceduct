import re
from html.parser import HTMLParser


class RemovePunctuation():

    """
    Removes following punctuation signs: '"’`\'()*,-.;[\\]^_{|}~“”/:?…\n\xA0'
    (where '\xA0' is Unicode for space)

    >>> normalize('abcd*()')
    >>> 'abcd'

    """

    PATTERN = re.compile('[%s]' % re.escape(u'"’`\'()*,-.;[\\]^_{|}~“”\xA0\n/:?…'))

    def normalize(self, data):
        return RemovePunctuation.PATTERN.sub(' ', data)


class Parser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.start_sponsored=False
        self.end_sponsored=False
        self.sponsored = ""
        self.content = ""
        self.remove_punctuation = RemovePunctuation()

    def _isdigit(self, txt):
        txtlen = float(len(txt))
        numlen = float(len([c for c in txt if c.isnumeric()]))

        return (numlen / txtlen) > 0.5

    def handle_starttag(self, tag, attrs):
        pass

    def handle_data(self, data):
        data = data.strip()
        dlen = len(data)
        if dlen == 0:
            return

        if data == '·':
            if not self.start_sponsored:
                self.start_sponsored = True
            else:
                self.end_sponsored = True
            return

        if dlen < 2 and self.start_sponsored and not self.end_sponsored:
            self.sponsored += "{} ".format(data.replace(',', ';'))
            return

        if dlen < 20:
            if self._isdigit(data):
                return

        if data in ["Like", "Comment", "Share"]:
            return

        data = self.remove_punctuation.normalize(data)

        if len(data) > 0:
            self.content += " {}".format(data.lower())

    def handle_endtag(self, tag):
        pass

    def reset_snippet_values(self):
        self.content = ''
        self.sponsored = ''
        self.start_sponsored = False
        self.end_sponsored = False
