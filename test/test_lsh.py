import math
import operator
import random
import unittest

from faceduct.lsh import MinHashSignature, LSH

WORDS = ['the', 'com', 'learned', '0:58', 'greg', 'simple', 'pictures', '251',
        'anyone', 'system', 'is', 'compartir', 'gusta', 'any', '0:04', '30',
        'skills', 'over', 'have', 'great', 'candid', 'be', 'compartido', 'me',
        'comentarios', 'and', '4', 'ver', 'comentar', 'más', 'years', 'a',
        'photography', 'developed', '149', 'in', 'mil', 'scenario', 'williams',
        'i', 'for', 'skillsfaster', 'delivering', 'by', 'veces', 'quick',
        'can']

WORD_INDEX = list(range(len(WORDS)))


def jaccard_sim(X: set, Y: set) -> float:
    """Jaccard similarity between X and Y."""
    return len(X & Y) / float(len(X | Y))


def rnd_post_fn(seed=1):
    random.seed(seed)

    def _rndpost():
        """Return a random post."""
        postlen = random.randint(20, 50)

        return ' '.join([WORDS[random.choice(WORD_INDEX)] for _ in range(postlen)])

    return _rndpost


def rnd_set_fn(seed=1):
    random.seed(seed)
    def _rndset():
        """Return a random set.  These values of n and k have wide-ranging
        similarities between pairs.
        """
        n = random.randint(20, 50)
        return tuple(set(random.randint(1, 100) for _ in range(n)))

    return _rndset

class TestMinHashSignature(unittest.TestCase):
    def setUp(self):
        self.rndset = rnd_set_fn()


    def test_signature_length(self):
        """Minhash signatures should have set dimension/length"""
        dim = 200
        mh = MinHashSignature(dim=200)
        rep = 100
        for _ in range(rep):
            self.assertEqual(len(mh.sign(self.rndset())), dim)


    def test_consistent_signature(self):
        """Minhash signatures should be consistent"""
        mh = MinHashSignature(dim=200)
        rep = 100
        for _ in range(rep):
            s = self.rndset()
            self.assertEqual(mh.sign(s), mh.sign(s))


    def test_signature_similarity(self):
        """
        The probability that two sets' signatures match at some index
        should be equal to the Jaccard similarity between the two.
        """
        to_str = lambda s: " ".join([str(x) for x in s])

        dim = 100
        n_tests = 100
        expected_error = 1 / math.sqrt(dim) # Expected error
        mh = MinHashSignature(dim)
        cum_err = 0.0
        # Compute cumulative error between jaccard and signature similarities
        for _ in range(n_tests):
            posts = (self.rndset(), self.rndset())
            signs = tuple(map(mh.sign, posts))
            jsim = jaccard_sim(*tuple(map(set, posts)))
            ssim = sum(list(map(operator.eq, *signs))) / float(dim)
            cum_err += abs(jsim - ssim)

        # Average error should be within the expected error
        self.assertTrue(cum_err / n_tests <= expected_error)


class TestLSH(unittest.TestCase):
    def setUp(self):
        self.rndpost = rnd_post_fn()


    def test_index_same(self):
        """
            Document indexing should be idempotent, i.e. a document should always
            end up in the same buckets, regardless of how many times is indexed
            and hyper-parameters the LSH instance is initialized with
        """
        # default LSH params
        lsh = LSH()
        post = self.rndpost()
        buckets = []
        for i in range(3):
            lsh.index('a', post)
            buckets.append(lsh.get_buckets(post))

        self.assertEqual(buckets[0], buckets[1])
        self.assertEqual(buckets[0], buckets[2])
        self.assertEqual(buckets[1], buckets[2])

        # conservative LSH params
        lsh = LSH(length=200, threshold=0.99, shingle_size=3)
        post = self.rndpost()
        buckets = []
        for i in range(3):
            lsh.index('a', post)
            buckets.append(lsh.get_buckets(post))

        self.assertEqual(buckets[0], buckets[1])
        self.assertEqual(buckets[0], buckets[2])
        self.assertEqual(buckets[1], buckets[2])

    def test_index(self):
        lsh = LSH()
        apost = self.rndpost()
        bpost = self.rndpost()
        lsh.index('a', apost)
        lsh.index('b', bpost)
        items = set()
        for band in lsh.lsh_index:
            self.assertTrue(len(band) >= 1)
            for _, idset in band.items():
                items |= idset

        self.assertTrue('a' in items)
        self.assertTrue('b' in items)


    def test_index_similar(self):
        """
            Two similar documents should be found in at least one
            bucket together, given the threshold and shingle_size.
        """
        apost = 'a b c d e f g h i j k l m n'
        bpost = 'b c d e f g h i j k l m n'

        # Should be similar
        for threshold, shingle_size in [(0.5, 3), (0.8, 2), (0.9, 1)]:
            lsh = LSH(threshold=threshold, shingle_size=shingle_size)
            lsh.index('a', apost)
            lsh.index('b', bpost)
            abuckets = set(h for _, h, _ in lsh.get_buckets(apost))
            bbuckets = set(h for _, h, _ in lsh.get_buckets(bpost))
            self.assertTrue(len(abuckets.intersection(bbuckets)) >= 1)

        # Shouldn't be similar
        for threshold, shingle_size in [(0.5, 14), (0.8, 12), (0.95, 3)]:
            lsh = LSH(threshold=threshold, shingle_size=shingle_size)
            lsh.index('a', apost)
            lsh.index('b', bpost)
            abuckets = set(h for _, h, _ in lsh.get_buckets(apost))
            bbuckets = set(h for _, h, _ in lsh.get_buckets(bpost))
            self.assertTrue(len(abuckets.intersection(bbuckets)) == 0)


    def test_index_dissimilar(self):
        """Two different documents shouldn't be found in any bucket together."""
        apost = 'a b c d e f g h i j k l m n'
        bpost = 'o p q r s t u v w x y z'

        # Should not be similar
        for threshold, shingle_size in [(0.1, 2), (0.25, 2), (0.5, 3), (0.8, 2), (0.9, 1)]:
            lsh = LSH(threshold=threshold, shingle_size=shingle_size)
            lsh.index('a', apost)
            lsh.index('b', bpost)
            abuckets = set(h for _, h, _ in lsh.get_buckets(apost))
            bbuckets = set(h for _, h, _ in lsh.get_buckets(bpost))
            self.assertTrue(len(abuckets.intersection(bbuckets)) == 0)


    def test_get_duplicates(self):
        """
            Two similar documents should be indexed as duplicates,
            given the threshold and shingle_size are not too conservative.
        """
        apost = 'a b c d e f g h i j k l m n'
        bpost = 'b c d e f g h i j k l m n'

        # Should be duplicates
        for threshold, shingle_size in [(0.5, 3), (0.8, 2), (0.9, 1)]:
            lsh = LSH(threshold=0.5, shingle_size=3)
            lsh.index('a', apost)
            lsh.index('b', bpost)
            dup = lsh.get_duplicates(apost)
            self.assertTrue('a' in dup and 'b' in dup)
            dup = lsh.get_duplicates(apost, 'a')
            self.assertTrue('a' not in dup and 'b' in dup)
            dup = lsh.get_duplicates(bpost)
            self.assertTrue('a' in dup and 'b' in dup)
            dup = lsh.get_duplicates(bpost, 'b')
            self.assertTrue('a' in dup and 'b' not in dup)


    def test_dump_and_load_index(self):
        lsh = LSH()
        apost = self.rndpost()
        bpost = self.rndpost()
        lsh.index('a', apost)
        lsh.index('b', bpost)

        lsh.dump()
        lsh.lsh_index.clear()
        self.assertTrue(len(lsh.lsh_index)==0)

        lsh.load()
        self.assertTrue(len(lsh.lsh_index)>=0)

        items = set()
        for band in lsh.lsh_index:
            self.assertTrue(len(band) >= 1)
            for _, idset in band.items():
                items |= idset

        self.assertTrue('a' in items and 'b' in items)
