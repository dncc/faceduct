import argparse
import os
import json
import gzip
import shutil
import time
import tqdm
import multiprocessing as mp
import multiprocessing.pool as mp_pool
from typing import Set

import html2txt
import lsh
from parser import Parser
from constants import JSON_EXT, HTML_EXT, HTML_EXT_LEN, HEADERS

REPORT = '''
Removing duplicates has finished! \x1b[m
========== REPORT =========
total # of posts: {num_total}
# of unique posts: {num_uniq}
# of duplicate posts: {num_dup}

Ratio unique/total : {ratio_uniq}%
Ratio duplicate/total : {ratio_dup}%

Total duration: {seconds} sec == {minutes} min == {hours} h
'''


def round_ratio(x, y, n=2):
    return round(100 * x / (y + 1e-8), n)


def print_report(num_total, num_dup, duration):
    num_uniq = num_total - num_dup
    print(
        REPORT.format(
            num_total=num_total,
            num_uniq=num_uniq,
            num_dup=num_dup,
            ratio_uniq=round_ratio(num_uniq, num_total),
            ratio_dup=round_ratio(num_dup, num_total),
            seconds=round_ratio(duration, 1),
            minutes=round_ratio(duration, 60),
            hours=round_ratio(duration, 3600),
        ))


def parse_args():
     parser = argparse.ArgumentParser(
        'Removes duplicates from Facebook posts in HTML and JSON (graph) format',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('input',
                        metavar='INPUT',
                        help='Input directory with Facebook posts in HTML and JSON format (.gz)')
     parser.add_argument('output',
                        metavar='OUTPUT',
                        help='Output directory, should contain unique Facebook posts in'
                        'HTML and JSON  format (.gz) and a CSV file with parsed text from'
                        'HTML files')
     parser.add_argument('-s',
                        '--similarity-threshold',
                        type=float,
                        default=0.4,
                        help='LSH similarity threshold (between 0 and 1). Check out:'
                        'http://infolab.stanford.edu/~ullman/mmds.html (section 3.4.2)')
     parser.add_argument('-w',
                        '--workers',
                        type=int,
                        default=2,
                        help='Number of workers')
     parser.add_argument('-m',
                        '--multiprocessing',
                        action='store_false',
                        default=True,
                        help='Flag to use multiprocessing or not, turned on '
                             'by default')

     return parser.parse_args()



if __name__ == "__main__":
    start = time.time()
    args = parse_args()

    worker_pool = mp_pool.ThreadPool(args.workers)
    if args.multiprocessing:
       worker_pool = mp.Pool(args.workers)

    # 1. Parse HTML files and save the output in 'snippets.csv'
    print("Parsing ...")
    html_parser = Parser()
    filenames = [f for f in os.listdir(args.input) if f.endswith(HTML_EXT)]
    progress_bar = tqdm.tqdm(filenames, desc='Dedup ', total=len(filenames))
    worker_args = [{'filename': filename, 'parser': html_parser, 'input': args.input}
        for filename in filenames]
    output_file = os.path.sep.join([args.output, 'snippets.csv'])
    result = html2txt.run_workers(worker_pool, worker_args, output_file, HEADERS, progress_bar)

    # 2. LSH indexing
    input_file = output_file
    output_file = os.path.sep.join([args.output, 'snippets.uniq.csv'])
    count, count_dup, uniq, lsh = lsh.index(input_file, output_file, threshold=args.similarity_threshold)
    print("Saving LSH index db to: {}".format(lsh.dump()))

    # 3. Copy unique files to the output directory
    print("Saving unique posts to {} ...".format(args.output))
    for filename in filenames:
        filename = filename[:-HTML_EXT_LEN]
        if filename not in uniq:
            continue

        htmlfile = os.path.sep.join([args.input, filename + HTML_EXT])
        shutil.copy(htmlfile, args.output)
        jsonfile = os.path.sep.join([args.input, filename + JSON_EXT])
        shutil.copy(jsonfile, args.output)

    end = time.time()
    duration = end - start
    print_report(count, count_dup, duration)
