import argparse
import os
import json
import gzip
import time
import tqdm
import multiprocessing as mp
import multiprocessing.pool as mp_pool

from parser import Parser
from constants import AD, NOAD, JSON_EXT, HTML_EXT, HTML_EXT_LEN, HEADERS

REPORT = '''
Parsing has finished successfully! \x1b[m
========== REPORT =========
total # of posts: {num_total}
# of parsed posts: {num_parsed}
# of skipped posts: {num_skipped}
# of ad posts: {num_ads}

Ratio parsed/total : {ratio_parsed_total}%
Ratio ads/parsed : {ratio_ads_parsed}%

Time: {seconds} sec == {minutes} min == {hours} h
'''


def worker(kwargs):
    filename = kwargs['filename']
    html_parser = kwargs['parser']
    input_dir = kwargs['input']

    success = False
    post = label = sponsored = ''
    is_ad = None
    filename = filename[:-HTML_EXT_LEN]
    jsonfile = os.path.sep.join([input_dir, filename + JSON_EXT])
    with gzip.open(jsonfile) as f:
        graph = json.loads(f.read())
        is_ad = graph['ad']

    if is_ad is None:
        print("No ad field: ", filename)
        return (success, filename, post, is_ad, sponsored)

    # extract text
    htmlfile = os.path.sep.join([input_dir, filename + HTML_EXT])
    with gzip.open(htmlfile, 'rt') as f:
        html_parser.feed(f.read())

    if len(html_parser.content) == 0:
        html_parser.reset_snippet_values()
        return (success, filename, post, is_ad, sponsored)

    success = True
    post = html_parser.content.strip()
    sponsored = html_parser.sponsored.strip()
    html_parser.reset_snippet_values()

    return (success, filename, post, is_ad, sponsored)


def round_ratio(x, y, n=2):
    return round(100 * x / (y + 1e-8), n)


def print_report(num_total, num_parsed, num_ads, duration):
    print(
        REPORT.format(
            num_total=num_total,
            num_parsed=num_parsed,
            num_skipped=num_total-num_parsed,
            num_ads=num_ads,
            ratio_parsed_total=round_ratio(num_parsed, num_total),
            ratio_ads_parsed=round_ratio(num_ads, num_parsed),
            seconds=round_ratio(duration, 1),
            minutes=round_ratio(duration, 60),
            hours=round_ratio(duration, 3600),
        ))

def run_workers(worker_pool, worker_args, output, headers, progress_bar):
    start = time.time()
    num_total = num_parsed = num_ads = 0
    with open(output, 'w') as output:
        output.write(','.join(headers) + '\n')
        for result in worker_pool.imap_unordered(worker, worker_args):
            num_total += 1
            success, filename, post, is_ad, sponsored = result
            if not success:
                continue
            num_parsed += 1
            num_ads += int(is_ad)
            label = AD if is_ad else NOAD
            row = [filename, post, label, sponsored]
            output.write(','.join(row) + '\n')
            output.flush()
            progress_bar.update()

    end = time.time()
    duration = end - start
    print_report(num_total, num_parsed, num_ads, duration)


def parse_args():
     parser = argparse.ArgumentParser(
        'Extract text from HTML Facebook posts',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
     parser.add_argument('input',
                        metavar='INPUT_DIR',
                        help='Input directory with JSON graphs')
     parser.add_argument('output',
                        metavar='OUTPUT',
                        help='Output file')
     parser.add_argument('-w',
                        '--workers',
                        type=int,
                        default=2,
                        help='Number of workers')
     parser.add_argument('-m',
                        '--multiprocessing',
                        action='store_false',
                        default=True,
                        help='Flag to use multiprocessing or not, turned on '
                             'by default')

     return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    worker_pool = mp_pool.ThreadPool(args.workers)
    if args.multiprocessing:
       worker_pool = mp.Pool(args.workers)

    html_parser = Parser()
    filenames = [f for f in os.listdir(args.input) if f.endswith(HTML_EXT)]
    progress_bar = tqdm.tqdm(filenames, desc='Parsing ', total=len(filenames))
    worker_args = [{'filename': filename, 'parser': html_parser, 'input': args.input}
            for filename in filenames]

    result = run_workers(worker_pool, worker_args, args.output, HEADERS, progress_bar)
