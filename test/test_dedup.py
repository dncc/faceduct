import os
import unittest
import tempfile
from faceduct.lsh import index


EXP_NUM_POS = 45
EXP_POS_DUP = 27
EXP_NUM_NEG = 8
EXP_NEG_DUP = 0


class TestDedup(unittest.TestCase):
    def setUp(self):
        self.tmp_dir = tempfile.gettempdir()
        self.inp_dir = os.path.sep.join([os.getcwd(), 'test', 'data'])


    def test_index_positives(self):
        pos_sample = os.path.sep.join([self.inp_dir, 'positives.csv'])
        pos_sample_out = os.path.sep.join([self.tmp_dir, 'positives.uniq.csv'])
        count, count_dup, uniq, lsh = index(pos_sample, pos_sample_out)

        self.assertEqual(count, EXP_NUM_POS)
        self.assertEqual(count_dup, EXP_POS_DUP)
        self.assertEqual(len(uniq), EXP_NUM_POS - EXP_POS_DUP)


    def test_index_negatives(self):
        neg_sample = os.path.sep.join([self.inp_dir, 'negatives.csv'])
        neg_sample_out = os.path.sep.join([self.tmp_dir, 'negatives.uniq.csv'])
        count, count_dup, uniq, lsh = index(neg_sample, neg_sample_out)

        self.assertEqual(count, EXP_NUM_NEG)
        self.assertEqual(count_dup, EXP_NEG_DUP)
        self.assertEqual(len(uniq), EXP_NUM_NEG - EXP_NEG_DUP)

